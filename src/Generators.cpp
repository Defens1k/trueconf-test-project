#include "Generators.h"
namespace generators {

    int rand_int(int left, int right) {
        return std::rand() % (right - left) + left;
    }

    int rand_int(unsigned int right) {
        return rand_int(0, right);
    }

    unsigned int rand_uint(unsigned int left, unsigned int right) {
        return rand_int(left, right);
    }

    unsigned int rand_uint(unsigned int right) {
        return rand_int(0, right);
    }


    generators::Rand_int_generator::Rand_int_generator(int left, int right) {
        std::srand(std::time(nullptr));
        left_range = left;
        right_range = right;
    }

    int generators::Rand_int_generator::get_value() const {
        return rand_int(left_range, right_range);
    }
}
