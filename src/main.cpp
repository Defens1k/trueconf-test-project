/* Необходимо:

1.заполнить случайными числами от 1 до 9 значения контейнеров vector[i] и map[i];

2.удалить случайное число элементов (не более 15) в каждом контейнере;

3.после этого провести синхронизацию, чтобы в vector и map остались только имеющиеся в обоих контейнерах элементы (дубликаты не удалять). */
#include "synchronize.h"

int main() {
    std::vector<int> vector;
    std::map<int, int> map;
    generators::Rand_int_generator digit_generator(1, 10); //генератор случайных чисел в нужном нам диапазоне

    fill_int_vector(vector, BEGIN_VECTOR_SIZE, digit_generator);
    fill_int_map(map, BEGIN_MAP_SIZE, digit_generator);    // заполняем контейнеры случайными числами

    print_vector(vector);   
    print_map(map);     


    delete_random_elements_from_container(vector, generators::rand_uint(DELETES_MAX_COUNT));    
    delete_random_elements_from_container(map,    generators::rand_uint(DELETES_MAX_COUNT));    // удаляем из контейнеров случайное количество элементов

    print_vector(vector);   
    print_map(map);

    synchronize(map, vector);    // выполняем синхронизацию

    print_vector(vector);   
    print_map(map);
}
