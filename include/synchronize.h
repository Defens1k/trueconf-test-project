#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <algorithm>

#include "Generators.h"

#define BEGIN_VECTOR_SIZE 20
#define BEGIN_MAP_SIZE    20       // начальные размеры контейнеров

#define DELETES_MAX_COUNT 15       // удаляем не более 15 элементов

template <typename T>
void print_vector(std::vector<T> v) {
    std::cout << "std::vector, size = " << v.size() << ", [";
    for (auto i : v) {
        std::cout << i << " ";
    }
    std::cout << "]"<< std::endl;
}

template <typename KEY, typename VALUE>
void print_map(std::map<KEY, VALUE> m) {
    std::cout << "std::map, size = " << m.size() << ", {";
    for (auto i : m) {
        std::cout << "["<< i.first << ": " << i.second << "] ";
    }
    std::cout << "}" << std::endl;
}

void fill_int_vector(std::vector<int> & vector, size_t count, const generators::Int_generator & generator) {  // заполняем вектор значениями из генератора
    vector.clear();
    for (size_t i = 0; i < count; i++) {
        vector.push_back(generator.get_value());
    }
}

void fill_int_map(std::map<int, int> & map, size_t count, const generators::Int_generator & generator) {  // заполням мапу значениями из генератора
    map.clear();
    for (size_t i = 0; i < count; i++) {
        map[i] = generator.get_value();
    }
}

template <typename T>
void delete_random_elements_from_container(T & container, size_t count) {    // удаляет count случайных элементов из контейнера
    std::cout << "delete " << count << " elements\n";
    if (count >= container.size()) {
        container.clear(); 
        return;
    }
    for (size_t i = 0; i < count; i++) {
        int pos_to_delete = generators::rand_int(container.size());   // итераторы не всех контейнеров поддерживают оператор "+" с числом
        auto iter = container.begin();                                // поэтому вручную в цикле вызываем оператор "++" нужное количество раз
        for (size_t j = 0; j < pos_to_delete; j++) {
            iter++;
        }
        container.erase(iter);
    }
}

void synchronize (std::map<int, int> & map, std::vector<int> & vector) {  
    std::cout << "Synchronization...\n";
    std::set<int> vector_uniq_set; // множество уникальных значений из вектора
    std::set<int> map_uniq_set;    // множество уникальных значений из мапы
    std::set<int> unique;

    for (auto i : vector) {
        vector_uniq_set.insert(i);
    }
    for (auto i : map) {
        map_uniq_set.insert(i.second);
    }

    std::set_intersection(vector_uniq_set.begin(), vector_uniq_set.end(),
                          map_uniq_set.begin(), map_uniq_set.end(),
                          std::inserter(unique, unique.begin()));   // находим пересечение множеств вектора и мапы и резултат записываем в unique

    for (auto i = vector.begin(); i != vector.end();) {   // удаляем элементы которых нет в unique
        if (unique.find(*i) == unique.end()) {
            std::cout << "erase " << *i << "from vector\n";
            i = vector.erase(i);     // erase возвращает указатель на следующий элемент, поэтому в цикле не нужно каждый раз инкрементировать i
        } else {
            i++;
        }
    }

    for (auto i = map.begin(); i != map.end();) {  // удаляем элементы которых нет в unique
        if (unique.find(i->second) == unique.end()) {
            std::cout << "erase " << i->second << "from map\n";
            i = map.erase(i);      // erase возвращает указатель на следующий элемент, поэтому в цикле не нужно каждый раз инкрементировать i
        } else {
            i++;
        }
    }
}

