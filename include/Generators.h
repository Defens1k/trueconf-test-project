#ifndef GENERATORS_H_
#define GENERATORS_H_
#include <cstdlib>
#include <ctime>
namespace generators {

    int rand_int(int, int); 
    int rand_int(unsigned int); 

    unsigned int rand_uint(unsigned int, unsigned int); 
    unsigned int rand_uint(unsigned int);


    class Int_generator {  // интерфейс генератора интовых чисел
     public:
         virtual int get_value() const = 0;
    };

    class Rand_int_generator : public Int_generator{  // генератор рандомных чисел из диапазона
     public:
         Rand_int_generator(int, int);
         int get_value() const;
     private:
         int left_range;
         int right_range;
    };
}
#endif //GENERATORS_H_
